# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='hocpy',
    version='1.1',
    description='Generates hits-of-code reports for a Git repository',
    long_description=readme,
    author='Dmitrii Pisarenko',
    author_email='dp@dpisarenko.com',
    url='https://bitbucket.org/altruix/hocpy/src/master/',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)
