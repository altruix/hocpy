class GitLogRecord:
    def __init__(self, added, deleted, path):
        self.added = added
        self.deleted = deleted
        self.path = path
