from .GitDataReader import GitDataReader
from .GitLogRecord import GitLogRecord
from .MainProjectInfo import MainProjectInfo
from .ReportGenerator import ReportGenerator


from datetime import date, timedelta, datetime
import sqlite3
import sys

class Main:
    def __init__(self):
        self.repo_path = "/Users/dp118m/dev/focus/legacy"
        self.db_path = "/Users/dp118m/dev/focus/diary3/legacy-hoc/db.db"
        self.raw_data_table = "HITS_OF_CODE"
        self.report_dir = "/Users/dp118m/dev/focus/diary3/legacy-hoc/"
        self.main_projects = []
        self.main_projects.append(MainProjectInfo("2020-10-17", "white-niggers", "White Niggers", "lit-universe/src/lit_universe/works/white_niggers"))
        self.beginning_of_time = date(2020, 9, 11)

    def run(self):
        today = date.today()
        print("Connecting to the database...")
        db_conn = sqlite3.connect(self.db_path)
        db_cursor = db_conn.cursor()
        self.create_raw_data_table(db_cursor, self.raw_data_table)
        print("Done")
        self.update_database(today, self.beginning_of_time, db_cursor)
        db_conn.commit()
        self.create_reports(today, db_cursor)
        db_conn.commit()
        db_cursor.close()
        db_conn.close()

    def read_newest_day_in_db(self, cursor):
        print("Determining the newest day in the database")
        sql = "SELECT MAX(DAY) FROM {raw_data_table}".format(raw_data_table=self.raw_data_table)
        return cursor.execute(sql).fetchone()[0]
    
    def create_raw_data_table(self, cursor, raw_data_table):
        cursor.execute("CREATE TABLE IF NOT EXISTS {raw_data_table} (DAY TEXT NOT NULL PRIMARY KEY, HOC INTEGER NOT NULL, DATA_INCOMPLETE INTEGER NOT NULL, WEEK TEXT, MONTH TEXT, MAIN_PROJECT_DIR TEXT, MAIN_PROJECT_HOC INTEGER)".format(raw_data_table=raw_data_table))

    def determine_days_to_update(self, beginning_of_time, today, newest_day_in_db):
        # range_start -- first day for which data need to be updated
        # range_end -- last day for which data need to be updated
        if newest_day_in_db == None:
            range_start = beginning_of_time
        else:
            newest_day_in_db_date = datetime.strptime(newest_day_in_db, "%Y-%m-%d").date()
            # We need to make sure that yesterday's data are updated
            range_start = newest_day_in_db_date - timedelta(days=1)
        range_end = today
        delta = range_end - range_start
        days_to_update = []
        for i in range(delta.days + 1):
            cur_day = range_start + timedelta(days=i)
            days_to_update.append(cur_day)
        return days_to_update

    def upsert_hits_of_code(self, day, today, cursor, main_project):
        git_data_reader = GitDataReader()
        git_data = git_data_reader.read_git_data(self.repo_path, day)
        git_log_records = self.extract_git_log_records(git_data)
        hits_of_code = self.calculate_hits_of_code(git_log_records)
        print("hits_of_code for {day}: {hits_of_code}".format(day=day, hits_of_code=hits_of_code))
        if main_project != None:
            main_project_dir = main_project.main_project_dir
            hits_of_code_main_project = self.calculate_hits_of_code_main_project(git_log_records, main_project_dir, main_project.main_project_dir2)
        else:
            hits_of_code_main_project = 0
            main_project_dir = ""
        incomplete_data = 1 if day == today else 0
        week_nr = day.isocalendar().week
        year = day.isocalendar().year
        week_nr_txt = "{:4d}-{:02d}".format(year, week_nr)
        month_nr = day.month
        month_nr_txt = "{:4d}-{:02d}".format(day.year, month_nr)
        sql = "INSERT INTO {table}(DAY, HOC, DATA_INCOMPLETE, WEEK, MONTH, MAIN_PROJECT_DIR, MAIN_PROJECT_HOC) VALUES('{day}', {hoc}, {incomplete}, '{week}', '{month}', '{main_project_dir}', {hits_of_code_main_project}) ON CONFLICT(DAY) DO UPDATE SET HOC={hoc}, DATA_INCOMPLETE={incomplete}, WEEK='{week}', MONTH='{month}', MAIN_PROJECT_DIR='{main_project_dir}', MAIN_PROJECT_HOC={hits_of_code_main_project};".format(table=self.raw_data_table, day=str(day), hoc=hits_of_code, incomplete=incomplete_data, week=week_nr_txt, month=month_nr_txt, main_project_dir=main_project_dir, hits_of_code_main_project=hits_of_code_main_project)
        print(sql)
        cursor.execute(sql)

    def remove_dash(self, txt):
        if txt == '-':
            return "0"
        else:
            return txt
        
    def extract_git_log_records(self, git_data):
        records = []
        for line in git_data.splitlines():
            parts = line.split("\t")
            added = int(self.remove_dash(parts[0]))
            deleted = int(self.remove_dash(parts[1]))
            path = parts[2]
            print("line: " + line)
            print("added: '" + str(added) + "', deleted: '" + str(deleted) + "', path: " + path)
            records.append(GitLogRecord(added, deleted, path))
        return records

    def calculate_hits_of_code(self, git_log_records):
        total = 0
        for cur_rec in git_log_records:
            total = total + cur_rec.added + cur_rec.deleted
        return total
    
    def calculate_hits_of_code_main_project(self, git_log_records, main_project_dir, main_project_dir2):
        total = 0
        for cur_rec in git_log_records:
            cur_path = cur_rec.path
            dir1 = "{main_project_dir}/".format(main_project_dir=main_project_dir)
            dir2 = "{main_project_dir2}/".format(main_project_dir2=main_project_dir2)
            if cur_path.startswith(dir1) or cur_path.startswith(dir2):
                total = total + cur_rec.added + cur_rec.deleted
        return total

    def update_database(self, today, beginning_of_time, cursor):
        print("Beginning of time: " + str(beginning_of_time))
        print("Current date: " + str(today))
        newest_day_in_db = self.read_newest_day_in_db(cursor)
        days_to_update = self.determine_days_to_update(beginning_of_time, today, newest_day_in_db)
        main_project = None
        for cur_day in days_to_update:
            cur_day_txt = str(cur_day)
            if (main_project == None):
                main_project = self.find_main_project(cur_day_txt)
            self.upsert_hits_of_code(cur_day, today, cursor, main_project)

    def find_main_project(self, cur_day_txt):
        if len(self.main_projects) == 1:
            first_prj = self.main_projects[0]
            if cur_day_txt >= first_prj.first_day:
                return first_prj
            else:
                return None
        else:
            print("Error: Cannot determine main project")
            return None

    def create_reports(self, today, db_cursor):
        main_project = self.find_main_project(str(today))
        rg = ReportGenerator(today, db_cursor, self.report_dir, main_project)
        rg.generate_reports()
    
