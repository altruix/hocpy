from datetime import timedelta
import os
import subprocess

class GitDataReader:
    def __init__(self):
        self.exclusion_patterns = []
        self.exclusion_patterns.append("*/target/*")
        self.exclusion_patterns.append("*.mp3")
        self.exclusion_patterns.append("*.ods")
        self.exclusion_patterns.append("*.odt")
        self.exclusion_patterns.append("*.docx")
        self.exclusion_patterns.append("*.epub")
        self.exclusion_patterns.append("*.html")
        self.exclusion_patterns.append("*.png")
        self.exclusion_patterns.append("*.pdf")
        self.exclusion_patterns.append("*.mm")
        self.exclusion_patterns.append("*.ott")
        self.exclusion_patterns.append("*.jpg")
        self.exclusion_patterns.append("*.jpeg")
        self.exclusion_patterns.append("*.JPG")
        self.exclusion_patterns.append("*.bpmn")
        self.exclusion_patterns.append("ui.ini")
        self.exclusion_patterns.append("*.checksum")
        self.exclusion_patterns.append("*.odg")
        self.exclusion_patterns.append("*.autosave")
        self.exclusion_patterns.append("*.backup")
        self.exclusion_patterns.append("recents.txt")
        self.exclusion_patterns.append("*.csv")
        self.exclusion_patterns.append("*.sql")
        self.exclusion_patterns.append("*.db")
        self.exclusion_patterns.append("*.sh")
        self.exclusion_patterns.append("*.rtf")
        self.exclusion_patterns.append("search.indexes")
        self.exclusion_patterns.append("version.txt")
        self.exclusion_patterns.append("*.scrivx")
        self.exclusion_patterns.append("*.toml")
        self.exclusion_patterns.append("user.lock")
        self.exclusion_patterns.append(".gitignore")

    def read_git_data(self, repo_path, day):
        pts = []
        pts.append("cd ")
        pts.append(repo_path)
        pts.append(" && ")
        pts.append("git log ")
        pts.append("--pretty=tformat: ")
        pts.append("--numstat ")
        pts.append("--ignore-space-change ")
        pts.append("--ignore-all-space ")
        pts.append("--ignore-submodules ")
        pts.append("--no-color ")
        pts.append("--find-copies-harder ")
        pts.append("-M ")
        pts.append("--diff-filter=ACDM ")
        pts.append("--since=\"")
        pts.append(str(day))
        pts.append(" 00:00\" ")
        pts.append("--before=\"")
        pts.append(str(day))
        pts.append(" 23:59\" ")
        pts.append(" -- .")

        for ptn in self.exclusion_patterns:
            pts.append(" ':(exclude){pattern}'".format(pattern=ptn))

        git_cmd = "".join(pts)
        print("git_cmd: " + git_cmd)
        result = os.popen(git_cmd).read()
        return result
        
