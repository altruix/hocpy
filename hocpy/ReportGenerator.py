from .TimeRelatedValue import TimeRelatedValue
from datetime import date, timedelta, datetime
from dateutil.relativedelta import *
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
import os

class ReportGenerator:
    def __init__(self, today, cursor, report_dir, main_project):
        self.today = today
        self.cursor = cursor
        self.report_dir = report_dir
        self.main_project = main_project

    def generate_reports(self):
        print("Generating reports...")
        font = {'family':'Courier Prime'}
        matplotlib.rc('font', **font)
        self.generate_reports2(True)
        self.generate_reports2(False)
        print("Generating reports completed")

    def generate_reports2(self, overall_stats):
        self.generate_text_summary(overall_stats)
        self.generate_this_week_hoc_bar_chart(overall_stats)
        self.generate_last_weeks_line_chart(overall_stats)
        self.generate_last_months_line_chart(overall_stats)
        self.generate_monthly_hoc_chart(overall_stats)
        self.generate_cumulative_hoc_chart(overall_stats)

    def generate_this_week_hoc_bar_chart(self, overall_stats):
        # Bar chart, days of current week on the x axis
        # Daily HOC on the y axis
        # Get the data
        week_nr_txt = self.compose_week_nr_txt()
        hoc_field = self.get_hoc_field(overall_stats)
        sql = """SELECT
    CASE strftime("%w", day)
        WHEN '1' THEN 'Monday'
        WHEN '2' THEN 'Tuesday'
        WHEN '3' THEN 'Wednesday'
        WHEN '4' THEN 'Thursday'
        WHEN '5' THEN 'Friday'
        WHEN '6' THEN 'Saturday'
        WHEN '0' THEN 'Sunday'
        ELSE '?'
    END DAY_OF_WEEK,
    {hoc_field} 
FROM HITS_OF_CODE
WHERE WEEK='{week}'
ORDER BY DAY""".format(week=week_nr_txt, hoc_field=hoc_field)
        labels = []
        values = []
        rows = self.cursor.execute(sql).fetchall()
        for cur_row in rows:
            day = cur_row[0]
            hoc = cur_row[1]
            labels.append(day)
            values.append(hoc)
        x = np.arange(len(labels))
        width = 0.35
        # Create the chart
        fig, ax = plt.subplots()
        r = ax.bar(x, values, width)
        ax.set_title("Daily hits of code during the week {week}".format(week=week_nr_txt))
        ax.set_xticks(x)
        ax.set_xticklabels(labels)
        plt.ylabel('Daily hits of code')
        if overall_stats:
            file_name = "{report_dir}/overall_this_week_hoc_bar_chart.png".format(report_dir=self.report_dir)
        else:
            file_name = "{report_dir}/main_project_this_week_hoc_bar_chart.png".format(report_dir=self.report_dir)
        plt.savefig(file_name)

    def generate_last_weeks_line_chart(self, overall_stats):
        hoc_field = self.get_hoc_field(overall_stats)
        # Last four weeks (30 days)
        # Line chart with days on the x axis
        # Total weekly HOC on the y axis
        thirty_days_ago = self.today - timedelta(days=30)
        sql = """SELECT WEEK, SUM({hoc_field})
FROM HITS_OF_CODE
WHERE DAY >= '{thirty_days_ago}'
GROUP BY WEEK;""".format(thirty_days_ago=thirty_days_ago, hoc_field=hoc_field)
        labels = []
        values = []
        rows = self.cursor.execute(sql).fetchall()
        for cur_row in rows:
            week = cur_row[0]
            hoc = cur_row[1]
            labels.append(week)
            values.append(hoc)
        fig, ax = plt.subplots()
        ax.plot(labels, values)
        ax.set_title("Last Weeks' Hits of Code")
        plt.ylabel('Weekly hits of code')
        plt.xlabel('Time [weeks]')
        if overall_stats:
            file_name = "{report_dir}/overall_last_weeks_line_chart.png".format(report_dir=self.report_dir)
        else:
            file_name = "{report_dir}/main_project_last_weeks_line_chart.png".format(report_dir=self.report_dir)
        plt.savefig(file_name)

    def generate_monthly_hoc_report(self, months_to_go_back, chart_title, image_path, overall_stats):
        hoc_field = self.get_hoc_field(overall_stats)
        months = []
        for i in range(0, months_to_go_back):
            day_in_past = self.today + relativedelta(months=-i)
            cur_month = day_in_past.month
            cur_year = day_in_past.year
            month_txt = "'{:4d}-{:02d}'".format(cur_year, cur_month)
            months.append(month_txt)
        months_txt = ", ".join(months)
        sql = """SELECT MONTH, SUM({hoc_field})
FROM HITS_OF_CODE
GROUP BY MONTH
HAVING MONTH IN ({months_txt})
ORDER BY MONTH;""".format(months_txt=months_txt, hoc_field=hoc_field)
        rows = self.cursor.execute(sql).fetchall()
        labels = []
        values = []
        for cur_row in rows:
            month = cur_row[0]
            hoc = cur_row[1]
            labels.append(month)
            values.append(hoc)
        fig, ax = plt.subplots()
        ax.plot(labels, values)
        ax.set_title(chart_title)
        plt.ylabel('Monthly hits of code')
        plt.xlabel('Time [months]')
        plt.savefig(image_path)
        
    def generate_last_months_line_chart(self, overall_stats):
        hoc_field = self.get_hoc_field(overall_stats)
        # 1. Like generate_last_weeks_line_chart but for
        # last three months.
        # 2. Line chart with days on the x axis
        # 3. Total weekly HOC on the y axis
        day_in_past = self.today + relativedelta(months=-3)
        sql = """SELECT DAY, WEEKLY_HOCS.WEEKLY_HOC
FROM HITS_OF_CODE
JOIN
(SELECT WEEK, SUM({hoc_field}) WEEKLY_HOC
FROM HITS_OF_CODE
WHERE DAY >= '{day_in_past}'
GROUP BY WEEK) as WEEKLY_HOCS
ON HITS_OF_CODE.WEEK = WEEKLY_HOCS.WEEK
WHERE DAY >= '{day_in_past}';""".format(day_in_past=day_in_past, hoc_field=hoc_field)
        labels = []
        values = []
        rows = self.cursor.execute(sql).fetchall()
        for cur_row in rows:
            day = cur_row[0]
            hoc = cur_row[1]
            labels.append(day)
            values.append(hoc)
        fig, ax = plt.subplots()
        ax.plot(labels, values)
        ax.set_title("Last Months' Hits of Code")
        ax.xaxis.set_major_locator(ticker.AutoLocator())
        ax.xaxis.set_minor_locator(ticker.AutoMinorLocator())
        plt.ylabel('Weekly hits of code')
        plt.xlabel('Time [days]')
        if overall_stats:
            file_name = "{report_dir}/overall_last_months_line_chart.png".format(report_dir=self.report_dir)
        else:
            file_name = "{report_dir}/main_project_last_months_line_chart.png".format(report_dir=self.report_dir)
        plt.savefig(file_name)

    def generate_cumulative_hoc_chart(self, overall_stats):
        hoc_field = self.get_hoc_field(overall_stats)
        sql = """SELECT
    DAY,
    {hoc_field} AS DAILY_HOC,
    SUM({hoc_field}) OVER (ROWS UNBOUNDED PRECEDING)
FROM HITS_OF_CODE
ORDER BY DAY;""".format(hoc_field=hoc_field)
        labels = []
        cumulated_hoc = []
        daily_hoc = []
        rows = self.cursor.execute(sql).fetchall()
        for cur_row in rows:
            day = cur_row[0]
            hoc = cur_row[1]
            cur_cumulated_hoc = cur_row[2]
            daily_hoc.append(hoc)
            labels.append(day)
            cumulated_hoc.append(cur_cumulated_hoc)
        fig = matplotlib.figure.Figure()
        ax1 = fig.add_subplot()
        ax1.set_title("Cumulated and Daily Hits of Code")
        line_cumulated = ax1.plot(labels, cumulated_hoc, color='red', label="Cumulated")
        ax1.xaxis.set_major_locator(ticker.AutoLocator())
        ax1.xaxis.set_minor_locator(ticker.AutoMinorLocator())
        ax1.set_xlabel("Time [days]")
        ax1.set_ylabel("Cumulated hits of code")
        ax2 = ax1.twinx()
        line_daily = ax2.plot(daily_hoc, color='green', linewidth=0.25, label="Daily")
        ax2.set_ylabel("Daily hits of code")
        # Add legend
        legend_lines = line_cumulated + line_daily
        labs = [l.get_label() for l in legend_lines]
        ax1.legend(legend_lines, labs, loc=0)
        if overall_stats:
            file_name = "{report_dir}/overall_cumulated_hoc_chart.png".format(report_dir=self.report_dir)
        else:
            file_name = "{report_dir}/main_project_cumulated_hoc_chart.png".format(report_dir=self.report_dir)
        fig.savefig(file_name)
        # 1. Mandatory: Cumulative HOC from beginning
        # of time to now (first y axis)
        # 2. Optional: Second y axis -- daily HOC
        # 3. x axis -- days

    def generate_monthly_hoc_chart(self, overall_stats):
        # 1. Monthly HOC during the last 12 months
        # 2. x axis -- months
        # 3. y axis -- total monthly HOC
        if overall_stats:
            file_name = "{report_dir}/overall_monthly_hoc_chart.png".format(report_dir=self.report_dir)
        else:
            file_name = "{report_dir}/main_project_monthly_hoc_chart.png".format(report_dir=self.report_dir)
        self.generate_monthly_hoc_report(12, "Last Months' Hits of Code", file_name, overall_stats)

    def generate_text_summary(self, overall_stats):
        pts = []
        if overall_stats:
            pts.append("OVERALL STATISTICS")
            pts.append(os.linesep)
            pts.append("==================")
            pts.append(os.linesep)
            pts.append(os.linesep)
        else:
            title = "MAIN PROJECT '{name}' STATISTICS".format(name=self.main_project.name).upper()
            pts.append(title)
            pts.append(os.linesep)
            pts.append("="*len(title))
            pts.append(os.linesep)
            pts.append(os.linesep)


        # 1. This should be a text file
        # 2. Make sure it is opened in the Emacs dashboard.
        # 3. It should contain summary data such as:
        # 3.1. All-time high (daily HOC)
        daily_all_time_high = self.calculate_daily_all_time_high(overall_stats)
        self.append_time_related_metric(pts, daily_all_time_high)

        # 3.2. All-time high (weekly HOC)
        weekly_all_time_high = self.calculate_weekly_all_time_high(overall_stats)
        self.append_time_related_metric(pts, weekly_all_time_high)

        # 3.3. All-time high (monthly HOC)
        monthly_all_time_high = self.calculate_monthly_all_time_high(overall_stats)
        self.append_time_related_metric(pts, monthly_all_time_high)

        # 3.4. Today's HOC
        today_hoc = self.calculate_today_hoc(overall_stats)
        self.append_time_related_metric(pts, today_hoc)

        # 3.5. This week's HOC
        this_week_hoc = self.calculate_this_week_hoc()
        self.append_time_related_metric(pts, this_week_hoc)
        # 3.6. This month's HOC
        this_month_hoc = self.calculate_this_month_hoc(overall_stats)
        self.append_time_related_metric(pts, this_month_hoc)

        # 3.7. Average HOC per day
        this_week_avg_hoc_per_day = self.calculate_this_week_avg_hoc_per_day(overall_stats)
        self.append_time_related_metric(pts, this_week_avg_hoc_per_day)

        # 3.8. HOC for every day of current week
        self.append_daily_hocs_this_week(pts)

        txt =  "".join(pts)

        if overall_stats:
            file_name = "{report_dir}/overall_text_summary.txt".format(report_dir=self.report_dir)
        else:
            file_name = "{report_dir}/main_project_text_summary.txt".format(report_dir=self.report_dir)

        report_file = open(file_name, "w")
        report_file.write(txt)
        report_file.close()

    def append_time_related_metric(self, pts, metric):
        pts.append(metric.name)
        pts.append(": ")
        pts.append(str(metric.value))
        pts.append(" (")
        pts.append(metric.time_value)
        pts.append(")")
        pts.append(os.linesep)

    def calculate_today_hoc(self, overall_stats):
        hoc_field = self.get_hoc_field(overall_stats)
        sql = """SELECT {hoc_field}
FROM HITS_OF_CODE
WHERE DAY='{today}';""".format(today=self.today, hoc_field=hoc_field)
        data = self.cursor.execute(sql).fetchone()
        hoc = data[0]
        return TimeRelatedValue("Today's hits of code", str(self.today), hoc)

    def compose_week_nr_txt(self):
        week_nr = self.today.isocalendar().week
        year = self.today.isocalendar().year
        return "{:4d}-{:02d}".format(year, week_nr)
    
    def calculate_this_week_hoc(self):
        week_nr_txt = self.compose_week_nr_txt()
        sql = """SELECT SUM(HOC)
FROM HITS_OF_CODE
WHERE WEEK='{week}'""".format(week=week_nr_txt)
        data = self.cursor.execute(sql).fetchone()
        hoc = data[0]
        return TimeRelatedValue("This week's hits of code", "week {week} (YYYY-WW)".format(week=week_nr_txt), hoc)

    def get_hoc_field(self, overall_stats):
        if overall_stats:
            hoc_field = "HOC"
        else:
            hoc_field = "MAIN_PROJECT_HOC"
        return hoc_field
    
    def calculate_daily_all_time_high(self, overall_stats):
        hoc_field = self.get_hoc_field(overall_stats)
        sql = """SELECT DAY, {hoc_field} 
FROM HITS_OF_CODE
WHERE HOC = (SELECT MAX({hoc_field}) FROM HITS_OF_CODE);""".format(hoc_field=hoc_field)
        data = self.cursor.execute(sql).fetchone()
        day = data[0]
        hoc = data[1]
        return TimeRelatedValue("All-time high (daily HOC)", day, hoc)

    def calculate_weekly_all_time_high(self, overall_stats):
        hoc_field = self.get_hoc_field(overall_stats)
        sql = """SELECT WEEK, SUM({hoc_field}) AS WEEKLY_HOC
FROM HITS_OF_CODE
GROUP BY WEEK
ORDER BY WEEKLY_HOC DESC;""".format(hoc_field=hoc_field)
        data = self.cursor.execute(sql).fetchone()
        week = data[0]
        hoc = data[1]
        return TimeRelatedValue("All-time high (weekly HOC)", "week {week} (YYYY-WW)".format(week=week), hoc)

    def calculate_monthly_all_time_high(self, overall_stats):
        hoc_field = self.get_hoc_field(overall_stats)
        sql = """SELECT MONTH, SUM({hoc_field}) MONTHLY_HOC
FROM HITS_OF_CODE
GROUP BY MONTH
ORDER BY MONTHLY_HOC DESC;""".format(hoc_field=hoc_field)
        data = self.cursor.execute(sql).fetchone()
        month = data[0]
        hoc = data[1]
        return TimeRelatedValue("All-time high (monthly HOC)", "month {month} (YYYY-MM)".format(month=month), hoc)

    def calculate_this_month_hoc(self, overall_stats):
        hoc_field = self.get_hoc_field(overall_stats)
        year = self.today.year
        month_nr = self.today.month
        month_nr_txt = "{:4d}-{:02d}".format(year, month_nr)
        sql = """SELECT SUM({hoc_field})
FROM HITS_OF_CODE
WHERE MONTH='{month}';""".format(month=month_nr_txt, hoc_field=hoc_field)
        data = self.cursor.execute(sql).fetchone()
        hoc = data[0]
        return TimeRelatedValue("This month's HOC", "month {month} (YYYY-MM)".format(month=month_nr_txt), hoc)

    def calculate_this_week_avg_hoc_per_day(self, overall_stats):
        hoc_field = self.get_hoc_field(overall_stats)
        week_nr_txt = self.compose_week_nr_txt()
        sql = """SELECT ROUND(AVG({hoc_field})-0.5,2)
FROM HITS_OF_CODE
WHERE WEEK='{week}';""".format(week=week_nr_txt, hoc_field=hoc_field)
        data = self.cursor.execute(sql).fetchone()
        hoc = max([data[0], 0])
        return TimeRelatedValue("This week's average hits of code per day", "week {week} (YYYY-WW)".format(week=week_nr_txt), hoc)

    def append_daily_hocs_this_week(self, pts):
        pts.append(os.linesep)
        pts.append("THIS WEEK'S DAILY HITS OF CODE")
        pts.append(os.linesep)
        pts.append("==============================")
        pts.append(os.linesep)
        pts.append(os.linesep)

        week_nr_txt = self.compose_week_nr_txt()

        sql = """SELECT
    CASE strftime("%w", day)
        WHEN '1' THEN 'Monday'
        WHEN '2' THEN 'Tuesday'
        WHEN '3' THEN 'Wednesday'
        WHEN '4' THEN 'Thursday'
        WHEN '5' THEN 'Friday'
        WHEN '6' THEN 'Saturday'
        WHEN '0' THEN 'Sunday'
        ELSE '?'
    END DAY_OF_WEEK,
    HOC 
FROM HITS_OF_CODE
WHERE WEEK='{week}'
ORDER BY DAY""".format(week=week_nr_txt)
        rows = self.cursor.execute(sql).fetchall()
        for cur_row in rows:
            day = cur_row[0]
            hoc = cur_row[1]
            pts.append(day.ljust(10))
            pts.append(": ")
            pts.append(str(hoc).rjust(4))
            pts.append(os.linesep)
        pts.append(os.linesep)
